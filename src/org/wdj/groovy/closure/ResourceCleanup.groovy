package org.wdj.groovy.closure
/**
 * 使用闭包进行资源清理
 */
class Resource {
    def open() {
        print "opened..."
    }

    def close() {
        print "closed"
    }

    def read() {
        print "read..."
    }

    def write() {
        print "write..."
    }

    /**
     * Execute Around Method模式（执行环绕方法）
     * 接收一个块作为参数，把对该块的调用夹到那对方法的调用之间
     * @param closure
     * @return
     */
    def static use(closure) {
        def r = new Resource()
        try {
            r.open()
            closure(r)
        } finally {
            r.close()
        }
    }
}
//使用者忘记调用close()，导致资源没有关闭
def resource = new Resource()
resource.open()
resource.read()
resource.write()
println ''
//使用闭包方式实现资源自动关闭
Resource.use {
    res ->
        res.read()
        res.write()
}