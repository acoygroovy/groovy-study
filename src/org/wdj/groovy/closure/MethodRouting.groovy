package org.wdj.groovy.closure

class Handler {
    def f1() {
        println "f1 of Handler called ... "
    }

    def f2() {
        println "f2 of Handler called ... "
    }
}

class Example {
    def f1() {
        println "f1 of Example called ... "
    }

    def f2() {
        println "f2 of Example called ... "
    }

    def foo(Closure closure) {
//        closure.delegate = new Handler()//有副作用，当该闭包还被用于其他的函数或线程中
//        closure()

        def clone = closure.clone()//复制闭包
        clone.delegate = new Handler()
        clone()

//        new Handler().with closure//完全由Handler中的方法代替
    }
}

def f1() {
    println "f1 of Script called ... "
}
//比包内的方法调用首先被路由到闭包的上下文对象---this，如果没找到这些方法，则路由到delegate
new Example().foo {
    f1()
    f2()
}