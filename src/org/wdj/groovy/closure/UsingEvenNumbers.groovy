package org.wdj.groovy.closure
/**
 * 闭包的便利性
 */
// 1、传统方式
// 求1到某个特定的数n之间所有偶数的和
def sum(n) {
    total = 0
    for (int i = 2; i <= n; i += 2) {
        total += i
    }
    total
}

println "Sum of even numbers from 1 to 10 is ${sum(10)}"

// 求1到某个特定的数n之间所有偶数的积
def product(n) {
    prod = 1
    for (int i = 2; i <= n; i += 2) {
        prod *= i
    }
    prod
}

println "Product of even numbers from 1 to 10 is ${product(10)}"

// 求1到某个特定的数n之间偶数的平方
def sqr(n) {
    squared = []
    for (int i = 2; i <= n; i += 2) {
        squared << i**2
    }
    squared
}

println "Squares of even numbers from 1 to 10 is ${sqr(10)}"

// 2、Groovy方式
// 高阶函数：以函数为参数或返回一个函数作为结果的函数，Groovy称这种匿名代码块为闭包
// Groovy的闭包不能单独存在，只能附到一个方法上，或是赋值给一个变量
def pickEven(n, block) {
    for (int i = 2; i <= n; i += 2) {
        block(i)
    }
}
// 变量block保存了一个指向闭包的引用，可以像传递对象一样传递闭包
pickEven(10, { println it })
// 闭包是最后一个参数，可写成如下调用方式
pickEven(10) { println it }
//如果只向代码块中传递一个参数，那么可以使用这个it这个特殊变量名来指代该参数，或者指定参数名
pickEven(10) { evenNumber -> println evenNumber }//evenNumber指代在pickEven()方法内传递给该闭包的实参？形参？

total = 0
pickEven(10) { total += it }
println "Sum of even numbers from 1 to 10 is ${total}"

product = 1
pickEven(10) { product *= it }
println "Product of even numbers from 1 to 10 is ${product}"
