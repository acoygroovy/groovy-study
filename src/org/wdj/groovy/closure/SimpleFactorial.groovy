package org.wdj.groovy.closure
/**
 * 使用尾递归编写程序
 */
def factorial1(BigInteger number) {
    if (number == 1) {
        1
    } else {
        number * factorial1(number - 1)
    }
}

try {
    println "factorial of 5 is ${factorial1(5)}"
    println "Number of bits in the result is ${factorial1(5000).bitCount()}"
} catch (Throwable ex) {
    println "Caught ${ex.class.name}"
}

def factorial2
factorial2 = {
    int number, BigInteger theFactorial ->
        number == 1 ? theFactorial : factorial2.trampoline(number - 1, number * theFactorial)
}.trampoline()
println "factorial of 5 is ${factorial2(5, 1)}"
println "Number of bits in the result is ${factorial2(5000, 1).bitCount()}"

def factorial3(int factorialFor) {
    def tailFactorial
    tailFactorial = {
        int number, BigInteger theFactorial = 1 ->
            number == 1 ? theFactorial : tailFactorial.trampoline(number - 1, number * theFactorial)
    }.trampoline()
    tailFactorial(factorialFor)
}

println "factorial of 5 is ${factorial3(5)}"
println "Number of bits in the result is ${factorial3(5000).bitCount()}"
GroovyObject
GroovyInterceptable