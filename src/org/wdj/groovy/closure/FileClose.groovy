package org.wdj.groovy.closure
/**
 * 使用闭包进行资源清理
 */
writer = new FileWriter('output.txt')
writer.write('!')
//忘记调用writer.close()

//withWriter自动刷新（flush）并关闭流
new FileWriter('output.txt').withWriter {
    writer -> writer.write('a')
}//不再需要自己调用close()