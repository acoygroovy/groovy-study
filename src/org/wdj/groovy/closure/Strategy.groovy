package org.wdj.groovy.closure
/**
 * 闭包的使用方式
 */
def totalSelectValues(n, closure) {
    total = 0
    for (i in 1..n) {
        if (closure(i)) {
            total += i
        }
    }
    total
}
//定义方法调用的参数时即时创建闭包
print "Total of even numbers from 1 to 10 is "
println totalSelectValues(10) { it % 2 == 0 }
//将闭包赋值给变量并复用
def isOdd = { it % 2 != 0 }
print "Total of odd numbers from 1 to 10 is "
println totalSelectValues(10, isOdd)
