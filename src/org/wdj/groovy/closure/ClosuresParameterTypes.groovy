package org.wdj.groovy.closure

/**
 * 动态闭包
 */
//动态确定一个闭包期望的参数的类型
def examine(Closure closure) {
    println "$closure.maximumNumberOfParameters parameter(s) given:"
    for (aParameter in closure.parameterTypes) {
        println aParameter.name
    }
    println "--"
}
// 即便一个闭包没有使用任何形参，就像{}或{it}中这样，其实它也会接受一个参数（名字默认为it），如果调用者没有向闭包提供任何值，
// 则第一个形参（it）为null
examine() {}
examine() { it }
// 如果希望闭包完全不接受任何参数，必须使用{->}这种语法：在->之前没有形参，说明该闭包不接受任何参数
examine() { -> }
examine() { vall -> }
examine() { Date vall -> }
examine() { Date vall, val2 -> }
examine() { Date vall, String vall2 -> }
