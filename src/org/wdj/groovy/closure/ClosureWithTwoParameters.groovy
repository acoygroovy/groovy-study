package org.wdj.groovy.closure
/**
 * 向闭包传递参数
 */
def tellFortune(closure) {
    closure new Date("02/20/2018"), "Your day is filled with ceremony"
}
//传入参数多于一个，需要通过名字一一列出
tellFortune {
    date, fortune -> println "Fortune for ${date} is '${fortune}"
}
//可以在闭包中定义参数类型，也可以省略参数类型
tellFortune {
    Date date, fortune -> println "Fortune for ${date} is '${fortune}"
}
