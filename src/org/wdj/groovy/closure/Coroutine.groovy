package org.wdj.groovy.closure
/**
 * 闭包与协程
 */
// 调用一个函数或方法会在程序的执行序列中创建一个新的作用域，在一个入口点（方法最上面的语句）进入函数，在该方法完成之后，
// 回到调用者的作用域
// 协程支持多个入口点，每个入口点都是上次挂起调用的位置。
// 1、进入一个函数，执行部分代码，挂起
// 2、回调调用者的上下文或作用域内执行一些代码
// 3、在挂起的地方恢复该函数的执行
def iterate(n, closure) {
    1.upto(n) {
        println Thread.currentThread().getName()
        println "In iterate with value ${it}"
        closure(it)
    }
}

println "Calling iterate"
total = 0
// 在java中，wait()和notify()与多线程结合使用，可以协助实现协程。闭包会让人产生“协程是在一个线程中执行”的错觉
iterate(4) {
    total += it
    println Thread.currentThread().getName()
    println "In closure total so far is ${total}"
}
println "Done"