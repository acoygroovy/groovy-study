package org.wdj.groovy.closure
/**
 * 动态闭包
 */
//判断一个闭包是否提供
def doSomeThing(Closure closure) {
    if (closure) {
        closure()
    } else {
        println "Using default implentation"
    }
}

doSomeThing { println "Use specialized implementation" }
doSomeThing()
