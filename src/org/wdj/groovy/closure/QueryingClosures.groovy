package org.wdj.groovy.closure

/**
 * 动态闭包
 */
//动态确定一个闭包期望的参数的数目
def completeOrder(amount, Closure taxComputer) {
    tax = 0
    if (taxComputer.maximumNumberOfParameters == 2) {//期望传入税率
        tax = taxComputer(amount, 6.05)
    } else {//使用默认税率
        tax = taxComputer(amount)
    }
    println "Sales tax is ${tax}"
}

completeOrder(100) { it * 0.0825 }
completeOrder(100) {
    amount, rate -> amount * (rate / 100)
}

