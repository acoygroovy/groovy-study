package org.wdj.groovy.closure
/**
 * 闭包的使用方式
 */
//闭包的复用
class Equipment {
    def calculator

    Equipment(calculator) {
        this.calculator = calculator
    }

    def simulate() {
        println "Running simulation"
        calculator()
    }
}

def eq1 = new Equipment({ println "Calculator 1" })//传递内联闭包
def aCalculator = {println "Calculator 2"}
def eq2 = new Equipment(aCalculator)
def eq3 = new Equipment(aCalculator)
eq1.simulate()
eq2.simulate()
eq3.simulate()