package org.wdj.groovy.closure
/**
 * 科里化闭包
 */
// 带有预绑定形参的闭包叫做科里化闭包（Curried Closure）
// 可以科里化任意多个形参，但这些形参必须是连续若干个。如果有n个形参，可以任意科里化K个，其中0 <= k < n
def tellFortunes(Closure closure) {
    Date date = new Date("09/20/2018")
    closure date, "Your day is filed with ceremony", "weidejin"
    closure date, "They're features, not bugs", "weidejin"

    println "-------curry()-------"
    // 当对一个闭包调用curry()时，就是要求预先绑定某些形参，在预先绑定了一个形参之后，调用闭包是就不必重复为这个形参提供实参了
    postFortune = closure.curry(date)
    postFortune "Your day is filed with ceremony", "weidejin"
    postFortune "They're features, not bugs", "weidejin"
    postFortune = closure.curry(date, "Your day is filed with ceremony")
    postFortune

    println "-------rcurry()-------"
    //使用rcurry()方法科里化后面的形参
    postFortune = closure.rcurry("Your day is filed with ceremony", "weidejin")
    postFortune date

    println "-------ncurry()-------"
    //使用ncurry()方法科里化位于形参列表中间的形参，传入要进行科里化的形参的位置，同时提供相应的值
    postFortune = closure.ncurry(1, "Your day is filed with ceremony")
    postFortune date, "weidejin"
    postFortune = closure.ncurry(1, "Your day is filed with ceremony", "weidejin")
    postFortune date
}

tellFortunes {
    date, fortune, author -> println "Fortune for ${date} is '${fortune},author: ${author}"
}