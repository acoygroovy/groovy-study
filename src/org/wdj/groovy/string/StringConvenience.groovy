package org.wdj.groovy.string
/**
 * 字符串便捷方法
 */
// -=操作符（Groovy在String类上添加的minus()方法）：将左侧字符串中与右侧字符串相匹配的部分去掉
// 类似的还有其他便捷方法：plus()（+）、multiply()（*）、next()（++）、replaceAll()和tokenize()
str = "It's a rainy day in Seattle"
println str
str -= "rainy "
println str
