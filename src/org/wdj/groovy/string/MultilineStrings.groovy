package org.wdj.groovy.string
/**
 * 多行字符串
 */
//通过将字符串包含在3个单引号内（'''...'''）定义多行字面常量
memo = '''Several of you raised concerns about long meetings.
To discuss this, we will be holding a 3 hour meeting atarting
at 9AM tomorrow. All getting this memo are required to attend.
If you can't make it, please have a meeting with your manager to explain.
'''
println memo
//使用3个双引号创建包含表达式的多行字符串
price = 251.12
message = """We're vary pleased to announce
that our stock price hit a high of \$${price} pre share
on December 24th. Great news in time for...
"""
println message