package org.wdj.groovy.string
/**
 * 正则表达式
 */
//~操作符可以方便地创建RegEx模式，这个操作符映射到String类的negate()方法
//也可以使用正斜杠、单引号或双引号来创建RegEx。正斜杠不必对反斜杠进行转义，如/\d*\w*/与"\\d*\\w*"等价
obj = ~"hello"
println obj.getClass().name

// 一对操作符：=~和==~
// =~部分匹配，==~精确匹配
pattern = ~"(G|g)roovy"
text = 'Groovy is Hip'
if (text =~ pattern)
    println "match"
else
    println "no match"
if (text ==~ pattern)
    println "match"
else
    println "no match"

// =~操作符返回一个Matcher对象，是一个java.util.regex.Matcher实例。
matcher = 'Groovy is groovy' =~ /(G|g)roovy/
print "Size of matcher is ${matcher.size()} "
println "with elements ${matcher[0]} and ${matcher[1]}"

//使用replaceFirst()或replaceAll()方法方便地替换匹配的文本
str = 'Groovy is groovy, really groovy'
println str
result = (str =~ /groovy/).replaceFirst('hip')
println result
result = (str =~ /groovy/).replaceAll('hip')
println result
