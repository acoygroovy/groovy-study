package org.wdj.groovy.string
/**
 * GString的惰性求值问题
 */
what = new StringBuffer('fence')
text = "The cow jumped over the $what"
println what.hashCode()
println text
what.replace(0, 5, "moon")
println what.hashCode()
println text
// 修改what引用
what = new StringBuffer('fence')
println what.hashCode()
println text
// 不能修改GString实例所绑定的内容
price = 684.71
company = 'Google'
quote = "Today $company stock closed at $price"
println quote
stocks = [Apple: 663.01, Microsoft: 30.95]
stocks.each {
    key, value ->
        company = key
        price = value
        println quote
}
// 如果GString中包含的是一个闭包，而非变量，该闭包就会被调用
// 如果闭包接收一个参数，GString会把Write对象当做一个参数发送给它
// 如果闭包不接收任何参数，GString会简单地调用该闭包，并打印我们想返回给Writer的结果
// 如果闭包接收的参数不止一个，调用则会失败，并抛出一个异常，所以别这么做
companyClosure = {
    it.write(company)
}
priceClosure = {
    it.write("${price}")
}
//当表达式需要求值/打印时，GString会调用闭包
quote = "Today ${companyClosure} stock closed at ${priceClosure}"
stocks.each {
    key, value ->
        company = key
        price = value
        println quote
}
//闭包没有参数，可以去掉it参数，GString会使用返回的内容
companyClosure = {
    -> company
}
priceClosure = {
    -> price
}
quote = "Today ${companyClosure} stock closed at ${priceClosure}"
stocks.each {
    key, value ->
        company = key
        price = value
        println quote
}
//自包含代码
quote = "Today ${-> company} stock closed at ${-> price}"
stocks.each {
    key, value ->
        company = key
        price = value
        println quote
}