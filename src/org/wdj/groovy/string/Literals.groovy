package org.wdj.groovy.string
/**
 * 字面常量与表达式
 */
//Groovy使用单引号创建字符串字面常量，单引号和双引号都是String类的实例
//java中，单引号是一个char，双引号才是一个String对象
//字符串字面常量也可以放双引号
println 'He said, "That is Groovy"'
str = 'A string'
println str.getClass().name
//显式创建一个字符，Groovy隐式地创建Character对象
ch = 'a' as char
println ch.getClass().name
//Groovy会把使用单引号的String看做一个纯粹的字面常量。因此，如果在里面放了任何表达式，Groovy并不会计算它们；
//相反，它就是按所提供的字面内容来使用它们。
value = 25
println 'The value is ${value}'

//Java的String是不可变的，Groovy也信守这种不可变性。一旦创建了一个String实例，就不能通过调用更改器等方法修改其内容
//使用[]操作符读取一个字符，不能修改
str = 'hello'
println str[2]
try {
    str[2] = '!'
} catch (MissingMethodException ex) {
    println ex.message
}

//使用双引号或正斜杠创建一个表达式
value = 12
println "He paid \$${value} for that."
//如果定义字符串时使用的是正斜杠，而非双引号，则不必转义$
println(/He paid $${value} for that./)
//如果表达式是一个像value这样的简单变量名，或者是一个简单的属性存取器（accessor），则包围表达式的{}是可选的
println(/He paid $$value for that./)

//惰性求值（lazy evaluation），把一个表达式保存在一个字符串中，稍后再打印
what = new StringBuffer('fence')
text = "The cow jumped over the $what"
println text
what.replace(0, 5, "moon")
println text

//使用单引号创建的字符串和使用双引号或正斜杠的字符串不同。前者是普通的java.lang.String，而后者有些特殊
def printClassInfo(obj) {
    println "class: ${obj.getClass().name}"
    println "superclass: ${obj.getClass().superclass.name}"
}
//Groovy并不会简单地因为使用了双引号或正斜杠就创建一个GString实例
val = 125
printClassInfo("The Stock closed at ${val}")
printClassInfo(/The Stock closed at ${val}/)
printClassInfo(/This is a simple String/)
printClassInfo("This is a simple String")