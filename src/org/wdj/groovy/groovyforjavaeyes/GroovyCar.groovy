package org.wdj.groovy.groovyforjavaeyes
//类名不能与文件名一样
class CarGroovy {
    def miles = 0
    final year

    CarGroovy(year) {
        this.year = year
    }
}

CarGroovy carGroovy = new CarGroovy(2018)
println "Year: $carGroovy.year"
println "Miles: $carGroovy.miles"
println "Setting miles"
carGroovy.miles = 25
println "Miles: $carGroovy.miles"

println ''

class CarGroovy1 {
    final year
    private miles = 0

    CarGroovy1(year) {
        this.year = year
    }

    def getMiles() {
        println "getMiles called"
        miles
    }
    /**
     * 不允许在类的外部对该属性的值进行任何修改
     * @param miles
     */
    private void setMiles(miles) {
        throw new IllegalAccessException("you're not allowed to change miles")
    }

    def drive(dist) {
        if (dist > 0) {
            miles += dist
        }
    }
}

def carGroovy1 = new CarGroovy1(2018)
println "Year: $carGroovy1.year"
println "Miles: $carGroovy1.miles"
println 'Driving'
carGroovy1.drive(10)
println "Miles: $carGroovy1.miles"
try {
    print 'Can I set the year? '
    carGroovy1.year = 2019
} catch (ReadOnlyPropertyException ex) {
    println ex.message
}
try {
    print 'Can I set the miles? '
    carGroovy1.miles = 12
} catch (IllegalAccessException ex) {
    println ex.message
}