package org.wdj.groovy.groovyforjavaeyes

def foo(str) {
    str?.reverse()
}

println foo('evil')
println foo(null)
