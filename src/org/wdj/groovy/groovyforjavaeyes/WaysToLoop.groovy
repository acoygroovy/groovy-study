package org.wdj.groovy.groovyforjavaeyes
/**
 * 实现循环的方式i
 */
//1、range
for (i in 0..2) {
    println i
}
//upto(),
// $it表示在这个上下文中，进行循环时的索引值
0.upto(2) {
    print "$it "
}
println ''
//times()
3.times {
    print "$it "
}
println ''
//step()
0.step(10, 2) {
    print "$it "
}
println ''

3.times { print 'ho ' }
println 'Merry Groovy!'