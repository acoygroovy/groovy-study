package org.wdj.groovy.groovyforjavaeyes
//发生异常，无需手动抛出异常
def openFile(fileName) {
    new FileInputStream(fileName)
}
//捕获指定异常
try {
    openFile("nonexistentfile")
} catch (FileNotFoundException ex) {
    println "Oops: " + ex
}
//捕获所有异常
try {
    openFile("nonexistentfile")
} catch (ex) {
    println "Oops: " + ex
}
//静态方法可以使用this来引用Class对象
class Wizard {
    def static learn(trick, action) {
        println this
        this
    }
}

Wizard.learn('alohomora', {/*...*/ })
        .learn('expelliarmus', {/*...*/ })
        .learn('lumos', {/*...*/ })