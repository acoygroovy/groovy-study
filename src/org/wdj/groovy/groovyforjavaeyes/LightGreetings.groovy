package org.wdj.groovy.groovyforjavaeyes

for (int i = 0; i < 3; i++) {
    System.out.print("ho ");
}
System.out.println("Merry Groovy!");

//使用Range对象的、更为轻量级的for循环方式，Groovy对括号很宽容
for(i in 0..2){
    print 'ho '
}
println 'Merry Groovy!'