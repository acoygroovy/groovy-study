package org.wdj.groovy.interceptingmethodsusingmop
/**
 * 使用MOP拦截方法
 */
//使用GroovyInterceptable拦截方法
class Car implements GroovyInterceptable {
    def check() {
        System.out.println "check called..."
    }

    def start() {
        System.out.println "start called..."
    }

    def drive() {
        System.out.println "drive called..."
    }

    @Override
    Object invokeMethod(String name, Object args) {
        System.out.print "call to $name intercepted..."
        if (name != 'check') {
            System.out.print "running filter..."
            Car.metaClass.getMetaMethod('check').invoke(this, null)
        }
        def validMethod = Car.metaClass.getMetaMethod(name, args)
        if (null != validMethod) {
            validMethod.invoke(this, args)
        } else {
            Car.metaClass.invokeMethod(this, name, args)
        }
    }
}

car = new Car()
car.start()
car.drive()
car.check()
try {
    car.speed()
} catch (Exception ex) {
    println ex
}
