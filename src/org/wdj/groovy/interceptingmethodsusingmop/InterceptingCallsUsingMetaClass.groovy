package org.wdj.groovy.interceptingmethodsusingmop
/**
 * 使用MetaClass拦截方法
 */
class CarMeta {
    def check() {
        System.out.println "check called..."
    }

    def start() {
        System.out.println "start called..."
    }

    def drive() {
        System.out.println "drive called..."
    }
}

CarMeta.metaClass.invokeMethod = {
    String name, args ->
        System.out.print "call to $name intercepted..."
        if (name != 'check') {
            System.out.print "running filter..."
            CarMeta.metaClass.getMetaMethod('check').invoke(delegate, null)
        }
        def validMethod = CarMeta.metaClass.getMetaMethod(name, args)
        if (null != validMethod) {
            validMethod.invoke(delegate, args)
        } else {
            CarMeta.metaClass.invokeMissingMethod(delegate, name, args)
        }
}

carMeta = new CarMeta()
carMeta.start()
carMeta.drive()
carMeta.check()
try {
    carMeta.speed()
} catch (Exception ex) {
    println ex
}

println Integer.metaClass.getClass().name
Integer.metaClass.invokeMethod = {
    String name, args ->
        System.out.println "call to $name intercepted on $delegate..."
        def validMethod = Integer.metaClass.getMetaMethod(name, args)
        if (null == validMethod) {
            Integer.metaClass.invokeMissingMethod(delegate, name, args)
        } else {
            System.out.println "running pre-filter..."
            result = validMethod.invoke(delegate, args)
            System.out.println "running post-filter..."
            result
        }
}
println 5.floatValue()
println 5.intValue()
try {
    println 5.empty()
} catch (Exception ex) {
    println ex
}

println Integer.metaClass.getClass().name