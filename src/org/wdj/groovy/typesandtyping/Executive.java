package org.wdj.groovy.typesandtyping;

import java.math.BigDecimal;

public class Executive extends Employee {
    @Override
    public void raise(Number amount) {
        System.out.println("Executive got raise");
    }

    public void raise(java.math.BigDecimal amount) {
        System.out.println("Executive got outlandish raise");
    }
}
