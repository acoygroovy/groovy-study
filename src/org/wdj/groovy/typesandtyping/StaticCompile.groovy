package org.wdj.groovy.typesandtyping

import groovy.transform.CompileStatic

@CompileStatic
def shout1(String str) {
    println str.toUpperCase()
}