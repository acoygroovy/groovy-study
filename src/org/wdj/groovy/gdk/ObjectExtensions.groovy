package org.wdj.groovy.gdk
/**
 * 使用Object类的扩展
 */
/**
 * 使用dump()和inspect(0方法
 */
str = 'hello'
println str
// dump()：类的一个实例包含哪些内容
println str.dump()
// inspect()：说明创建一个对象需要提供什么，如果类没有实现该方法，会简单地返回toString()所返回的内容
println str.inspect()

/**
 * 2、使用上下文with()方法
 */
list = [1, 2]
list.add(3)
list.add(4)
println list.size()
println list.contains(2)

list.with {
    add(3)
    add(4)
    println size()
    println contains(2)
}

list.with {
    println "this is ${this}"
    println "owner is ${owner}"
    println "delegate is ${delegate}"
}

/**
 * 3、使用sleep
 */
thread = Thread.start {
    println "Thread started"
    startTime = System.nanoTime()
    new Object().sleep(2000)
    endTime = System.nanoTime()
    println "Thread done in ${(endTime - startTime) / 10**9} seconds"
}
new Object().sleep(1000)
println "Let's interrupt that thread"
thread.interrupt()
thread.join()

def playWithSleep(flag) {
    thread = Thread.start {
        println "Thread started"
        startTime = System.nanoTime()
        new Object().sleep(2000) {
            println "Interrupted... " + it
            flag
        }
        endTime = System.nanoTime()
        println "Thread done in ${(endTime - startTime) / 10**9} seconds"
    }
    thread.interrupt()
    thread.join()
}

playWithSleep(true)
playWithSleep(false)

/**
 * 4、间接访问属性
 */
class Car {
    int miles, fuelLevel
}

car = new Car(fuelLevel: 80, miles: 25)
properties = ['miles', 'fuelLevel']
properties.each {
    name -> println "$name = ${car[name]}"
}
car[properties[1]] = 100
println "fuelLevel now is ${car.fuelLevel}"

/**
 * 5、间接调用方法
 */
class Person {
    def walk() {
        println "Walking..."
    }

    def walk(int miles) {
        println "Walking $miles miles..."
    }

    def walk(int miles, String where) {
        println "Walking $miles miles $where..."
    }
}

peter = new Person()
peter.invokeMethod("walk", null)
peter.invokeMethod("walk", 10)
peter.invokeMethod("walk", [2, 'uphill'] as Object[])