package org.wdj.groovy.gdk

class PriceExtension {
    public static double getPrice(String self) {
        def url = "http://ichart.finance.yahoo.com/table.csv?s=$self".toURL()
        def data = url.readLines()[1].split(",")
        Double.parseDouble(data[-1])
    }
}

class PriceStaticExtension {
    public static double getPrice(String selfType, String ticker) {
        def url = "http://ichart.finance.yahoo.com/table.csv?s=$ticker".toURL()
        def data = url.readLines()[1].split(",")
        Double.parseDouble(data[-1])
    }
}
