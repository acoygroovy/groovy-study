package org.wdj.groovy.gdk
/**
 * 其他扩展
 */
/**
 * 1、数组的扩展
 */
// 在所有数组类型上，比如int[]、double[]和char[]等，都可以使用Range对象作为索引
int[] arr = [1, 2, 3, 4, 5, 6]
println arr[2..5]

//process = "wc".execute()
//process.out.withWriter {
//    it << "Let the World know...\n"
//    it << "Groovy Rocks!\n"
//}
//println process.in.text
//println process.text

/**
 * 3、使用java.io的扩展
 */
//读取整个文件内容
println new File('ObjectExtensions.groovy').text
//读取一行处理一行
new File('ObjectExtensions.groovy').eachLine {
    line -> println line
}
//满足某个特定条件的内容行
println new File('ObjectExtensions.groovy').filterLine {
    it =~ /package/
}
//向文件中写入信息
new File('output.txt').withWriter {
    file -> file << "some data..."
}