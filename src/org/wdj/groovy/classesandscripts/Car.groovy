package org.wdj.groovy.classesandscripts

class Car {
    int year = 2008
    int miles

    String toString() {
        "InterceptingCalls: year: $year, miles: $miles"
    }
}
