package org.wdj.groovy.xml

import groovy.xml.StreamingMarkupBuilder

/**
 * 创建XML
 */
langs = ['C++': 'Stroustrup', 'Java': 'Gosling', 'Lisp': 'McCarthy']
content = ''
langs.each {
    language, author ->
        fragment = """
    <language name="${language}">
        <author>${author}</author>
    </language>
        """
        content += fragment
}
xml = """<languages>${content}
</languages>
"""
println xml


langs = ['C++': 'Stroustrup', 'Java': 'Gosling', 'Lisp': 'McCarthy']
xmlDocument = new StreamingMarkupBuilder().bind {
    mkp.xmlDeclaration()
    mkp.declareNamespace(computer: "Computer")
    languages {
        comment << "Created using StreamingMarkupBuilder"
        langs.each {
            key, value ->
                computer.language(name: key) {
                    author(value)
                }
        }
    }
}
println xmlDocument