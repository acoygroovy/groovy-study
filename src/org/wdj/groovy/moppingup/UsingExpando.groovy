package org.wdj.groovy.moppingup
/**
 * 使用Expando创建动态类
 */
carA = new Expando()
carB = new Expando(year: 2018, miles: 0)
carA.year = 2018
carA.miles = 10
println "carA: " + carA
println "carB: " + carB

car = new Expando(year: 2018, miles: 0, turn: { println 'turning...' })
car.drive = {
    miles += 10
    println "$miles miles driven"
}
car.drive()
car.turn()