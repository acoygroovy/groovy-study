package org.wdj.groovy.synthesiswithmop
/**
 * 使用methodMissing合成方法
 */
class Person1 implements GroovyInterceptable {
    def work() {
        "working..."
    }
    def plays = ['Tennis', 'VolleyBall', 'BasketBall']

    @Override
    Object invokeMethod(String name, Object args) {
        System.out.println "intercepting call for $name"
        def method = metaClass.getMetaMethod(name, args)
        if (method) {
            method.invoke(this, args)
        } else {
            metaClass.invokeMethod(this, name, args)
        }
    }

    def methodMissing(String name, args) {
        System.out.println "methodMissing called for $name"
        def methodInList = plays.find {
            it == name.split('play')[1]
        }
        if (methodInList) {
            def impl = {
                Object[] vargs ->
                    "playing ${name.split('play')[1]}..."
            }
            Person1 instance = this
            instance.metaClass."$name" = impl
            impl(args)
        } else {
            throw new MissingMethodException(name, Person1.class, args)
        }
    }
}
jack = new Person1()
println jack.work()
println jack.playTennis()
println jack.playTennis()
