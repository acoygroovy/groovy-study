package org.wdj.groovy.synthesiswithmop

class Person4 {
}

def emc = new ExpandoMetaClass(Person4)
emc.methodMissing = {
    String name, args ->
        "I'm Jack of all trades...I can $name"
}
emc.initialize()
def jack = new Person4()
def paul = new Person4()

jack.metaClass = emc
println jack.sing()
println jack.dance()
println jack.juggle()
try {
    paul.sing()
} catch (ex) {
    println ex
}