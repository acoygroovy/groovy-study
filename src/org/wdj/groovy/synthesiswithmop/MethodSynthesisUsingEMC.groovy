package org.wdj.groovy.synthesiswithmop

class Person2 {
    def work() {
        "working..."
    }
}

Person2.metaClass.methodMissing = {
    String name, args ->
        def plays = ['Tennis', 'VolleyBall', 'BasketBall']
        System.out.println "methodMissing called for $name"
        def methodInList = plays.find {
            it == name.split('play')[1]
        }
        if (methodInList) {
            def impl = {
                Object[] vargs ->
                    "playing ${name.split('play')[1]}..."
            }
            Person2.metaClass."$name" = impl
            impl(args)
        } else {
            throw new MissingMethodException(name, Person2.class, args)
        }
}
jack = new Person2()
println jack.work()
println jack.playTennis()
println jack.playTennis()
println jack.playVolleyBall()
println jack.playVolleyBall()
try {
    println jack.playPolitics()
} catch (ex) {
    println ex
}
