package org.wdj.groovy.synthesiswithmop

class Person3 {
    def work() {
        "working..."
    }
}

Person3.metaClass.invokeMethod = {
    String name, args ->
        System.out.println "intercepting call for $name"
        def method = Person3.metaClass.getMetaMethod(name, args)
        if (method) {
            method.invoke(delegate, args)
        } else {
            Person3.metaClass.invokeMissingMethod(delegate, name, args)
        }
}

Person3.metaClass.methodMissing = {
    String name, args ->
        def plays = ['Tennis', 'VolleyBall', 'BasketBall']
        System.out.println "methodMissing called for $name"
        def methodInList = plays.find {
            it == name.split('play')[1]
        }
        if (methodInList) {
            def impl = {
                Object[] vargs ->
                    "playing ${name.split('play')[1]}..."
            }
            Person3.metaClass."$name" = impl
            impl(args)
        } else {
            throw new MissingMethodException(name, Person3.class, args)
        }
}
jack = new Person3()
println jack.work()
println jack.playTennis()
println jack.playTennis()
println jack.playVolleyBall()
println jack.playVolleyBall()
try {
    println jack.playPolitics()
} catch (ex) {
    println ex
}
