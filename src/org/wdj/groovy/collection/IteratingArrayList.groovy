package org.wdj.groovy.collection
/**
 * 迭代ArrayList
 */
//1、使用List的each方法，这个迭代器也称内部迭代器
list = [1, 3, 4, 1, 8, 9, 2, 6]
list.each {
    println it
}
//使用reverseEach()方法反向迭代元素
list.reverseEach {
    println it
}
//使用eachWithIndex()方法迭代，其中包含索引和该索引下的值
list.eachWithIndex { int entry, int i ->
    println "index = $i entry = $entry"
}
//对闭包中的元素求和
total = 0
list.each {
    total += it
}
println "Total is $total"
//把集合中的每个元素变为原来的2倍
//使用<<操作符（映射到leftShift()方法）将所得的值放到结果中
doubled = []
list.each {
    doubled << it * 2
}
println doubled
//2、使用List的collect方法，这个迭代器也称内部迭代器
//collect()方法和each()一样，会在集合中的每个元素上调用传入的闭包。不过它会把闭包的返回值收集到一个集合中，最后返回这个
//生成的结果集合。闭包有一个隐式的return
println list.collect {
    it * 2
}