package org.wdj.groovy.collection

/**
 * 使用查找方法
 */
list = [4, 3, 1, 2, 4, 1, 8, 9, 2, 6]
//挑出集合中第一个等于2的元素
//find()会找到第一次出现的匹配对象，像each()方法一样，find()方法也会对集合进行迭代，但是它只会迭代到闭包返回true为止。
//一得到true，find()方法就会停止迭代，并将当前的元素返回，如果遍历结束也没有得到true，则返回null
println list.find {
    it == 2
}
//指定条件，找到列表中第一个大于4的数
println list.find {
    it > 4
}
//查找第一个匹配对象的位置
println list.findIndexOf {
    it == 2
}
//查找最后一个匹配对象的位置
println list.findLastIndexOf {
    it == 2
}
println list.findAll {
    it == 2
}
println list.findAll {
    it > 4
}