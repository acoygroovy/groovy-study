package org.wdj.groovy.collection
/**
 * 使用List
 */
// 在Groovy中创建一个java.util.ArrayList实例要比在Java中容易。
//不必使用new或知名类名，而是可以简单地列出想包含在List中的初始值
list = [1, 3, 4, 1, 8, 9, 2, 6]
println list
println list.getClass().name
//[]操作符可用于获取List中的元素
println list[0]
println list[list.size() - 1]
//使用负索引值，-1表示最后一个元素，以此类推
println list[-1]
println list[-2]
//使用Range对象获得集合中的几个连续的值
println list[2..5]
//Range使用负索引值
println list[-6..-3]
//使用Range作为索引，java.util.ArrayList会返回一个指向原来列表部分内容的实例副本（以前版本得到的并非副本）
subList = list[2..5]
println subList.dump()
subList[0] = 55
println "After subList[0] = 55 subList = $subList list = $list"