package org.wdj.groovy.collection
/**
 * List上的其他便捷方法
 */
//each()方法实现计算总的字符数
list = ['Programming', 'In', 'Groovy']
count = 0
list.each {
    count += it.size()
}
println count
//collect()和sum(0方法实现计算总的字符数
println list.collect {
    it.size()
}.sum()
//inject()会对集合中的每个元素调用闭包。
//集合中的元素是用参数element表示，inject()会把将要注入的一个初始值当做一个参数，并通过carryOver参数把它放到第一次对闭包
//的调用中。之后它会把从闭包获得的结果注入到随后对闭包的调用中。
println list.inject(0) {
    carryOver, element -> carryOver + element.size()
}
//连接元素。join()会迭代每个元素，然后将每个元素和作为输入参数给定的字符连接起来。
println list.join(' ')
//flatten()拉平
list[0] = ['Be', 'Productive']
println list
list = list.flatten()
println list
//-操作符（minus()）移除元素，如果移除的元素不存在，Groovy会忽略掉
println list - ['Productive', 'In']
println list - 'Be'
//使用reverse()方法得到列表的一份副本，其中的元素时反向排列的
println list.reverse()
//*展开操作符：在每个元素上执行操作，不用显式地使用迭代
println list.size()
println list*.size()//等同list.collect{it.size()}
def words(a, b, c, d) {
    println "$a $b $c $d"
}

words(*list)