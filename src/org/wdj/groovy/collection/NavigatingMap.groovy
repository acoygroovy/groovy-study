package org.wdj.groovy.collection
/**
 * 在Map上迭代
 * 闭包：如果只想要MapEntry，就使用一个参数，如果想分别获得键和值，则使用两个参数
 */
//1、Map的each方法
langs = ['C++': 'Stroustrup', 'Java': 'Gosling', 'Lisp': 'MaCarthy']
langs.each {
    entry -> println "Language $entry.key was authored by $entry.value"
}
langs.each {
    language, author -> println "Language $language was authored by $author"
}
//2、Map的collect方法
println langs.collect {
    entry -> entry.key.replace("+", "P")
}
println langs.collect {
    language, author -> language.replace("+", "P")
}
//3、Map的find和findAll方法，与ArrayList中的对应方法类似。
println "Looking for the first language with name greater than 3 characters"
entry = langs.find {
    language, author -> language.size() > 3
}
println "Found $entry.key written by $entry.value"

println "Looking for the first language with name greater than 3 characters"
selected = langs.findAll {
    language, author -> language.size() > 3
}
selected.each {
    key, value -> println "Found $key written by $value"
}
/**
 * Map上的其他便捷方法
 */
//确定集合中是否有任何元素满足某些条件，使用any()方法：查找至少一个满足给定条件的Map中的元素
print "Does any language name have a nonalphabetic character? "
println langs.any {
    language, author -> language =~ "[A-Za-z]"
}
//every()方法检查是否所有的元素都满足给定条件
print "Does all language names have a nonalphabetic character? "
println langs.every {
    language, author -> language =~ "[^A-Za-z]"
}
friends = [briang : 'Brian Goetz', brians: 'Brian Sletten',
           davidb : 'David Bock', davidg: 'David Geary',
           scottd : 'Scott Davis', Scottl: 'Scott Leberknight',
           stuarth: 'Stuart Halloway']
groupByFirstName = friends.groupBy {
    it.value.split(" ")[0]
}
println groupByFirstName
groupByFirstName.each {
    firstName, buddies ->
        println "$firstName: ${buddies.collect { key, fullName -> fullName }.join(', ')}"
}
//两个技巧：Groovy将Map用于具名参数，以及使用Map实现接口