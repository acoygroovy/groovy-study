package org.wdj.groovy.collection
/**
 * s使用Map类
 */
//通过使用闭包，Groovy使Map用起来更为简单和优雅。创建一个Map实例也很简单，不需要使用new或指明任何类型类名，只需简单地创建
//键值对即可
langs = ['C++': 'Stroustrup', 'Java': 'Gosling', 'Lisp': 'MaCarthy']
println langs.getClass().name
//使用[]操作符来访问一个键的值
println langs['Java']
println langs['C++']
//可以把键用作好像是Map的一个属性，以此访问该键对应的值
println langs.Java
//陷阱：不能再Map上调用class属性，Map会假定class这个名字指向的是一个（不存在的）键，而返回null
//当调用class属性时，Map和其他一些类的实例不会返回Class元对象，为避免结果出乎意料，在实例上总是使用getClass()方法，
// 而不是class属性
try {
    println langs.class.name//java.lang.NullPointerException
} catch (NullPointerException ex) {
    println ex.message
}
//操作符重载++，先获取langs.C的值，然后再++
try {
    println langs.C++//不合法代码，java.lang.NullPointerException: Cannot invoke method next() on null object
} catch (NullPointerException ex) {
    println ex.message
}
//把键当做一个String
println langs.'C++'
//定义一个Map时，对于规规矩矩的键名，可以省略其引号
langs = ['C++': 'Stroustrup', Java: 'Gosling', Lisp: 'McCarthy']
println langs.Java
println langs.'C++'