package org.wdj.groovy.injectionwithmop

class StringUtil {
    def static toSSN(self) {
        if (self.size() == 9) {
            "${self[0..2]}-${self[3..4]}-${self[5..8]}"
        }
    }
}

use(StringUtil) {
    println "123456789".toSSN()
    println new StringBuilder("987654321").toSSN()
}
try {
    println "123456789".toSSN()
} catch (MissingMethodException ex) {
    println ex.message
}

@Category(String)
class StringUtilAnnotated {
    def toSSN() {
        if (size() == 9) {
            "${this[0..2]}-${this[3..4]}-${this[5..8]}"
        }
    }
}

use(StringUtilAnnotated) {
    println "123456789".toSSN()
}