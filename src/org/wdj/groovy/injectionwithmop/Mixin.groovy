package org.wdj.groovy.injectionwithmop

/**
 * 使用Mixin注入方法
 */
class Friend {
    def listen() {
        "$name is listening as a friend"
    }
}

@groovy.lang.Mixin(Friend)
class PersonWithFriend {
//    static {
//        mixin Friend
//    }
    String firstName
    String lastName

    String getName() {
        "$firstName $lastName"
    }
}

john = new PersonWithFriend(firstName: "John", lastName: "Smith")
println john.listen()

class Dog {
    String name
}

Dog.mixin Friend
buddy = new Dog(name: "Buddy")
println buddy.listen()

class Cat {
    String name
}

try {
    rude = new Cat(name: "Rude")
    rude.listen()
} catch (ex) {
    println ex.message
}
socks = new Cat(name:"Socks")
socks.metaClass.mixin Friend
println socks.listen()