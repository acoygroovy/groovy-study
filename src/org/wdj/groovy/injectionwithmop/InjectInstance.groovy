package org.wdj.groovy.injectionwithmop
/**
 * 向具体的实例中注入方法
 */
class Person{
    def play(){
        println 'playing...'
    }
}
def emc = new ExpandoMetaClass(Person)
emc.sing = {
    -> 'oh baby baby...'
}
emc.initialize()

def jack = new Person()
def paul = new Person()
jack.metaClass = emc
println jack.sing()

paul.sing()

