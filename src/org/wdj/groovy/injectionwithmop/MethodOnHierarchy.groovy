package org.wdj.groovy.injectionwithmop

daysFromNow = {
    ->
    Calendar today = Calendar.instance
    today.add(Calendar.DAY_OF_MONTH, (int) delegate)
    today.time
}
Integer.metaClass.daysFromNow = daysFromNow
Long.metaClass.daysFromNow = daysFromNow
println 5.daysFromNow()
println 5L.daysFromNow()

Number.metaClass.someMethod = {
    -> println "someMethod called"
}
2.someMethod()
2L.someMethod()

Integer.metaClass.'static'.isEven = {
    val -> val % 2 == 0
}
println "Is 2 even? " + Integer.isEven(2)
println "Is 3 even? " + Integer.isEven(3)

Integer.metaClass.constructor << {
    Calendar calendar -> new Integer(calendar.get(Calendar.DAY_OF_YEAR))
}
println new Integer(Calendar.instance)
//新建构造器
Integer.metaClass.constructor = {
    int val ->
        println "Intercepting constructor call"
        constructor = Integer.class.getConstructor(Integer.TYPE)
        constructor.newInstance(val)
}
println new Integer(4)
//替换构造器
println new Integer(Calendar.instance)