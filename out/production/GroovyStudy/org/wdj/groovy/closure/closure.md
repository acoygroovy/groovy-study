# 闭包
## 1、闭包的便利性
## 2、闭包的应用
## 3、闭包的使用方式
## 4、向闭包传递参数
## 5、使用闭包进行资源清理
## 6、闭包与协程
## 7、科（柯）里化闭包
## 8、动态闭包
## 9、闭包委托
## 10、使用尾递归编写程序
## 11、使用记忆化改进性能